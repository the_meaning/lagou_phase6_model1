package com.lagou.homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqHomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqHomeworkApplication.class, args);
    }

}
