package com.lagou.homework.util;

import java.util.UUID;

/**
 * @author: lizequn
 * @time: 2021/7/19 20:13
 * @description:
 */
public class UuidUtil {
    public static String uuid() {
        return UUID.randomUUID().toString();
    }
}